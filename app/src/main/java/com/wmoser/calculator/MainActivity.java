package com.wmoser.calculator;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Button btn0, btn1, btn2 , btn3 , btn4 , btn5 , btn6,
            btn7 , btn8 , btn9 , btnAdd , btnSub , btnDiv ,
            btnMult , btnDec ,btnClr , btnEql ;
    float firstVal , secondVal ;
    boolean boolAdd , boolSub ,boolMult ,boolDiv ;
    EditText editText ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSub = (Button) findViewById(R.id.btnSub);
        btnDec = (Button) findViewById(R.id.btnDec);
        btnClr = (Button) findViewById(R.id.btnClr);
        btnEql = (Button) findViewById(R.id.btnEql);
        editText = (EditText) findViewById(R.id.editText);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "2");
            }
        });

       btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "4");
            }
        });

       btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "9");
            }
        });

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "0");
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (editText == null)
                {
                    editText.setText("");
                } else {
                    firstVal = Float.parseFloat(editText.getText() + "");
                   boolAdd = true;
                    editText.setText(null);
                }
            }

        });
           btnSub.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    firstVal = Float.parseFloat(editText.getText() + "");
                    boolSub = true ;
                    editText.setText(null);
                }
            }
        );
        btnMult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    firstVal = Float.parseFloat(editText.getText() + "");
                    boolMult = true ;
                    editText.setText(null);
                }
            });
 
        btnDiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    firstVal = Float.parseFloat(editText.getText() + "");
                    boolDiv = true ;
                    editText.setText(null);
                }
            });
 
        btnEql.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    secondVal = Float.parseFloat(editText.getText() + "");


                    if (boolMult == true){
                        editText.setText(firstVal * secondVal + "");
                        boolMult=false;
                    }

                    if (boolDiv == true){
                        editText.setText(firstVal / secondVal + "");
                        boolDiv=false;
                    }

                    if (boolAdd == true){

                        editText.setText(firstVal + secondVal + "");
                        boolAdd=false;
                    }


                    if (boolSub == true){
                        editText.setText(firstVal - secondVal + "");
                        boolSub=false;
                    }
                }
            });
 
        btnClr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText("");
                }
            });
 
        btnDec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editText.setText(editText.getText() + ".");
                }
            });
        }
    }
